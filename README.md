# Awesome Project Build with TypeORM

Steps to run this project:

1. Run `npm i` command
2. Setup database settings inside `ormconfig.json` file (make sure Postgres db is running )
3. Run `npm start` command

Or Run with docker-compose (docker-compose up)

**Goal:**
- Create node application from scratch with all features(roles, authorization, etc).


ORM
- TypeORM is being used in project for db query and creating entities.

Database
- Postgres

Web Server
- express

Entities
- User
- Role

Relation
- ManyToMany (USER might have many roles and a Role belong to many user  )

API path
- /api/auth/login  -> POST (for getting access token, credential is written in seedDB.ts file)
- /api/user        -> POST (for creating user)
- /api/user        -> GET  (for getting users)

Checking Authorization:
- middleware is applied on UserController
- user with Admin right can create user otherwise forbidden(403) will be return 

Checking Authentication:
- middleware is applied on UserController if not applied Unauthorized(401) will return
- token expiry time is 1hour (configurable) 

