const dotenv = require('dotenv');
dotenv.config();

module.exports = {
   "type": "postgres",
   "host": "localhost",
   "port": 5432,
   "username": "admiadmi",
   "password": "Postgres12!",
   "database": "weatherApp",
   "synchronize": true,
   "logging": false,
   "entities": [
      "src/entity/**/*.ts"
   ],
   "migrations": [
      "src/migration/**/*.ts"
   ],
   "subscribers": [
      "src/subscriber/**/*.ts"
   ],
   "cli": {
      "entitiesDir": "src/entity",
      "migrationsDir": "src/migration",
      "subscribersDir": "src/subscriber"
   }
}