import { Entity, Column, ManyToMany } from "typeorm";

import { BaseEntity } from "../core/BaseEntity";
import { User } from "./User";

@Entity()
export class Role extends BaseEntity {

    @Column()
    name: string;

    @Column()
    isActive: boolean;

    @ManyToMany(type => User, user=>user.roles)
    users:User[];
}
