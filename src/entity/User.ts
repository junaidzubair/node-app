import * as bcrypt from "bcryptjs";
import { IsNotEmpty } from "class-validator";
import { Entity, Column, Unique, ManyToMany, JoinTable } from "typeorm";

import { BaseEntity } from "../core/BaseEntity";
import { Role } from "./Role";

@Entity()
@Unique(["email"])
export class User extends BaseEntity {

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column({ length: 50 }) @IsNotEmpty()
    email:string;

    @Column({select:false})
    password:string;

    @Column()
    isActive:boolean;

    hashPassword(){
        this.password = bcrypt.hashSync(this.password,12);
    }

    isPasswordValid(password: string) {
        return bcrypt.compareSync(password, this.password);
    }

    @ManyToMany(type => Role, role => role.users,{cascade:true})
    @JoinTable()
    roles: Role[];

}
