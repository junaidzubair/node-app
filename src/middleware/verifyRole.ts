import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";

import { Role } from "../common/Role";
import BaseController from "../core/BaseController";
import { User } from "../entity/User";


export const verifyRole = (roles: Array<string>) => {
    return async (req: Request, res: Response, next: NextFunction) => {

        const email = res.locals.jwtPayload.email;
        
        const userRepo = getRepository(User);
        let user:User;
        try {
            user = await userRepo.findOneOrFail({ where: { email: email }, relations:["roles"]});
        } catch (error) {
            BaseController.unAuthorized(res,'Unauthorized');
            return;
        }
        
        let userRoles = user.roles.map((role: any) => role.name).find(x=>roles.indexOf(x)>-1);
        // let userCon = roles.find(role=>userRoles.indexOf(role)>-1);
        

        if(userRoles) {
            next();
            return;
        }

        BaseController.forbidden(res);
    }

}