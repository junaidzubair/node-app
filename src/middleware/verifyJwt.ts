import { NextFunction, Request, Response } from "express";
import * as jwt from "jsonwebtoken";

import config from "../config/config";
import BaseController from "../core/BaseController";


export const verifyJwt = async (req: Request, res: Response, next:NextFunction) => {

    const token = <string>req.headers['auth'];
    let jwtPayload;

    try {
        jwtPayload = jwt.verify(token, config.jwtSecret);
        res.locals.jwtPayload = jwtPayload;
    } catch(error) {
        console.log('verifyJWT----------->>>>> ', error);
        BaseController.unAuthorized(res,'Unauthorized');
        return;
    }

    next();
}