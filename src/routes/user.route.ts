import { Router } from "express";

import { Role } from "../common/Role";
import { UserController } from "../controller/UserController";
import { verifyJwt } from "../middleware/verifyJwt";
import { verifyRole } from "../middleware/verifyRole";

const router = Router();

router.post("/", [verifyJwt, verifyRole([Role.Admin])], UserController.create);
router.get("/", [verifyJwt, verifyRole([Role.Admin,Role.HR])], UserController.getAll);

export default router;