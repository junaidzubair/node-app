import {Router}  from "express";
import AuthController from "../controller/AuthController";

const router = Router();

router.get('/greeting', AuthController.getGreeting);
router.post('/login', AuthController.login);     

export default router;