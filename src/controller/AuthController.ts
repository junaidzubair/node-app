import { Request,Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";

import config from "../config/config";
import BaseController from "../core/BaseController";
import { User } from "../entity/User";

class AuthController  {

    static getGreeting (req: Request, res: Response){
        BaseController.success(res, { greeting: 'Hello World' });
    }
    
    static async login(req: Request, res: Response) {

        const { password, email } = req.body;

        if(!password || !email){
            BaseController.badRequest(res,"username or password is missing")
        }

        const userRepo = getRepository(User);
        let user: User;
        try {
             user = await userRepo
                            .createQueryBuilder("user")
                            .select(['user.id','user.email','user.firstName','user.lastName','user.password','user.isActive'])
                            .leftJoinAndSelect("user.roles", "role")
                            .where("user.email= :email", { email: email})
                            .getOneOrFail();
        
        } catch (error) {
            console.log('user login error', error);
            BaseController.unAuthorized(res,'Invalid email');
            return;
        }

        if(!user.isPasswordValid(password)){
            BaseController.unAuthorized(res,'Invalid password');
            return;
        }

        const token = jwt.sign({userId: user.id, email: user.email, fullName:`${user.firstName} ${user.lastName}`, roles: user.roles.map((role)=>role.name) }, config.jwtSecret, { expiresIn: config.expiresIn });
        BaseController.success(res, { token });            
    }
}

export default AuthController;