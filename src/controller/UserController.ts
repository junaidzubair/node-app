import { Console } from "console";
import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";

import config from "../config/config";
import BaseController from "../core/BaseController";
import { Role } from "../entity/Role";
import { User } from "../entity/User";

export class UserController {


    static create = async (req: Request, res: Response)=> {

        const { email,firstName,lastName, roles} = req.body;

        if (!firstName || !lastName || !email ) {
            BaseController.badRequest(res, "Object missing some properties");
            return;
        }

        const userRepo = getRepository(User);
        let user: User;
        try {
            user = new User();
            user.firstName = firstName;
            user.lastName = lastName;
            user.email = email;
            user.isActive =true;
            user.password = "admin123";
            user.hashPassword();

            
            const roleToAssign = await getRepository(Role).createQueryBuilder('role').where("role.name IN (:...names)",{names:roles}).getMany();
            user.roles= roleToAssign; 
            await userRepo.save(user);

        } catch (error) {
            console.log(error);
            BaseController.badRequest(res, error.message);
            return;
        }

        BaseController.success(res, { firstName,lastName,email, id:user.id });
        // BaseController.success(res, { firstName,roles:user.roles });
    }

    static getAll = async(req:Request, res:Response) => {
        try{
            const users = await getRepository(User).createQueryBuilder('user').leftJoinAndSelect("user.roles", "role").getMany(); 
            BaseController.success(res, users);
        } catch(error){
            BaseController.badRequest(res);
            console.error(error);
        }
    }
}
