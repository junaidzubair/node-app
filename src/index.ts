import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as cors from "cors";
import * as bodyParser from "body-parser";

import routes from "./routes";
import { SeedDB } from "./config/seedDB";
import config from "./config/config";

let port = config.port || 8000;

createConnection().then(async connection => {

    const app = express();

    // Call midlewares
    app.use(cors());
    app.use(bodyParser.json());

    
    // Seed db with initial data
    const seedDB = new SeedDB();
    seedDB.seedData();

    
    //Set all routes from routes folder
    app.use("/api", routes);
    
    app.listen(port, () => {
        console.log("Server started on port " + port);
    });

}).catch(error => console.log(error));
