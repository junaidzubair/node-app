
import { Response } from "express";

class BaseController {
     
    static forbidden(response: Response<any, Record<string, any>>, message:string = "Forbidden") {
         response.status(403).send({
             meta:{
                 message: message,
                 status: 403
             },
             data:null
         });
     }

    static badRequest(response: Response<any, Record<string, any>>, message: string ="Bad request") {
         response.status(400).send({
             meta: {
                 message: message,
                 status: 400
             },
             data:null
         });
     };

     static unAuthorized(response: Response<any, Record<string, any>>,message: string= 'UnAuthorized') {
         response.status(401).send({
             meta: {
                 message: message,
                 status: 401
             },
             data:null
         })
     };


     static success = (response:Response, data:any=null, message:string="success") => {
        response.status(200).send({
            meta:{
                message:message,
                data:data
            }
        })
    };
}

export default BaseController;