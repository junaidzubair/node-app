import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class BaseEntity {

    @PrimaryGeneratedColumn("increment")
    id:number;

    @Column({default: () => 'CURRENT_TIMESTAMP', select: false })
    createdAt: Date;

    @Column({ nullable: true, select: false})
    lastModified: Date;
}