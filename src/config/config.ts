import * as dotenv from "dotenv";
dotenv.config();

const config ={
    jwtSecret: process.env.JWT_SECRET,
    expiresIn: process.env.EXPIRES_IN,
    port:process.env.PORT
};

export default config;