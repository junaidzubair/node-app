import { getRepository } from "typeorm"
import { Role } from "../entity/Role"
import { User } from "../entity/User";


export class SeedDB {
    private roleEntity = getRepository(Role);
    private userEntity = getRepository(User);
    
    seedData = () => {
        this.seedRole();  
        this.seedUser();  
    }

    async seedUser  () {
        const userExist = await this.userEntity.findOne({ email: "junaid.zubair@yopmail.com" });
        if (!userExist) {
            const user = new User();
            user.firstName = "Junaid";
            user.lastName = "Shaikh";
            user.email = "junaid.zubair@yopmail.com"
            user.isActive = true;
            user.password = "junaid";
            user.hashPassword();

            const userRole = new Role();
            userRole.name = "Admin";
            userRole.isActive = true;

            user.roles = [ userRole ];

            try{
                await this.userEntity.save(user);
            }catch(e){
                console.log('Coudn\'t saved user ', e);
            }
            
        }
    }

    async seedRole(){
        const rolesExist = await this.roleEntity.findOne({ name: "PM" });
        if (!rolesExist) {
            // const admin = new Role();
            // admin.isActive = true;
            // admin.name = "Admin";
            
            const pm = new Role();
            pm.isActive = true;
            pm.name = "PM";

            const hr = new Role();
            hr.isActive = true;
            hr.name = "HR";

            const developer = new Role();
            developer.isActive = true;
            developer.name = "Developer";

            const roles = [ pm, hr, developer];
            
            try {
                await this.roleEntity.save(roles);
                console.log('Roles create');
            } catch (e) {
                console.log('Coudn\'t saved roles ', e);
            }
        }
    }
}